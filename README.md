## Hello, I'm [Sangam Singh!](https://codersangam.com) 👋

<p align="left"> <img src="https://komarev.com/ghpvc/?username=codersangam&label=Views&color=green&style=plastic" alt="iamSangam" /> </p>


<a href="https://www.twitter.com/codersangam">
  <img align="left" alt="Sangam's Twitter" width="100px" height="28px" src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" />
</a>
<a href="https://www.linkedin.com/in/sangam-singh-1b21941a0/">
  <img align="left" alt="Sangam's Linkedin" width="100px" height="28px" src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />
</a>
<a href="https://t.me/codersangam">
  <img align="left" alt="Sangam's Telegram" width="100px" height="28px" src="https://img.shields.io/badge/Telegram-1DA1F2?style=for-the-badge&logo=telegram&logoColor=white&color=darkblue" />
</a>
<a href="https://instagram.com/codersangam">
  <img align="left" alt="Sangam's Instagram" width="100px" height="28px" src="https://img.shields.io/badge/Instagram-1DA1F2?style=for-the-badge&logo=instagram&logoColor=white&color=red" />
</a>
<a href="https://www.facebook.com/people/Ronnie/100026804131628/">
  <img align="left" alt="Sangam's Facebook" width="100px" height="28px" src="https://img.shields.io/badge/Facebook-1DA1F2?style=for-the-badge&logo=facebook&logoColor=white&color=blue" />
</a>


<br/>
<br/>


- 👀 I’m interested in developing mobile apps, AI and newer Technologies.
- 🌱 I’m currently learning Flutter.
- 💬 Ask me about Java, Python, Laravel or any tech-related stuff.
- 📫 How to reach me: [Portfolio - @codersangam](https://codersangam.com)
- ⚡ Fun fact: I nearly spent most of the time to learn new Technologies.
- 🔫 My Weapon: MacBook Air 


[![Linkedin: Sangam](https://img.shields.io/badge/-sangam-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/sangam-singh-1b21941a0/)](https://img.shields.io/badge/-sangam-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/sangam-singh-1b21941a0/)
[![GitHub codersangam](https://img.shields.io/github/followers/codersangam?label=follow&style=social)](https://github.com/codersangam)
[![website](https://img.shields.io/badge/PortfolioWebsite-Sangam-2648ff?style=flat-square&logo=google-chrome)](https://codersangam.com)


**Languages and Tools:**  

<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/flutter/flutter.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/dart/dart.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/android/android.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/ios/ios.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/wordpress/wordpress.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/laravel/laravel.png"></code>    
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/java/java.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/firebase/firebase.png"></code> 
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png"></code>
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png"></code> 
<code><img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/bootstrap/bootstrap.png"></code> 

<div align="left">
<a href="https://github.com/codersangam">
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=codersangam&theme=dracula&hide_langs_below=1" />
</a>
<a href="https://github.com/codersangam">
 <img align="center" src="https://github-readme-stats.vercel.app/api?username=codersangam&show_icons=true&theme=midnight-purple&line_height=27&count_private=true" alt="Sangam's Github Stats"/>
</a>
<a href="https://github.com/codersangam/flutter-basics">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=codersangam&repo=flutter-basics&theme=nightowl" />
</a>
  <a href="https://github.com/codersangam/instagram_clone">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=codersangam&repo=instagram_clone&theme=nightowl" />
</a>
</div>

<br/>

**Statistics**
<div align="left">
<code><img height="500" src="https://wakatime.com/share/@4818b327-9c94-4397-85a6-c82a5027f03c/e1157c5e-8254-4708-91d0-481b2de77858.svg"></code>
</div>


<div align="center">

### Show some love. ⭐️  my repositories!

</div>

